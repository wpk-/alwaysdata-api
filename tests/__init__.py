#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Unit tests for alwaysdata_api.

Usage:  python -m unittest
"""
from unittest import TestCase, main
from unittest.mock import MagicMock, patch

from alwaysdata_api import DEFAULT_AUTH, Domain, Record, Resource, Site,\
    api_root


class MockResponse:
    def __init__(self, text: str = None, json_data=None):
        self.text = text
        self.json_data = json_data

    def json(self):
        return self.json_data

    def raise_for_status(self):
        pass


class GoodResource(Resource):
    prop: str
    href: str = 'good/'


class BadResource(Resource):
    prop: str
    # defines no `href`.


class TestResource(TestCase):
    def setUp(self):
        self.goodie = GoodResource()
        self.baddie = BadResource()

        mock_response = MockResponse('abcd', {'prop': 'val'})

        patcher = patch('alwaysdata_api.requests.request',
                        return_value=mock_response)

        self.request_mock = patcher.start()
        self.addCleanup(patcher.stop)

    # HTTP methods
    # ============

    def test_delete(self):
        # DELETE requires the instance `href` to be set.
        with self.assertRaises(ValueError):
            self.goodie.put()
        # It sends a DELETE request to the correct URL.
        self.goodie.href = 'good/123'
        self.goodie.delete()
        self.request_mock.assert_called_with('DELETE',
                                             api_root + 'good/123',
                                             auth=DEFAULT_AUTH)

    def test_get(self):
        # GET constructs the instance href from the class and the ID.
        # The returned value is an instance of the correct class.
        res = GoodResource.get(123)
        self.request_mock.assert_called_with('GET',
                                             api_root + 'good/123',
                                             auth=DEFAULT_AUTH)
        self.assertIsInstance(res, GoodResource)
        self.assertIn(('prop', 'val'), res.__dict__.items())

    def test_list(self):
        # JSON returns a list here.
        # The request is made to the class href.
        with patch('alwaysdata_api.requests.request',
                   return_value=MockResponse('', [{'prop': 'val'}])) as mock:
            lst = GoodResource.list(prop='val')
        mock.assert_called_with('GET', api_root + 'good/',
                                params={'prop': 'val'}, auth=DEFAULT_AUTH)
        self.assertEqual(len(lst), 1)
        self.assertIsInstance(lst[0], GoodResource)
        self.assertIn(('prop', 'val'), lst[0].__dict__.items())

    def test_patch(self):
        # PATCH requires the resource location in the data.
        with self.assertRaises(ValueError):
            self.goodie.put()
        # All attributes are submitted to the instance href.
        # (To create a patch object, see `delta`.)
        self.goodie.href = 'good/123'
        self.goodie.patch()
        self.request_mock.assert_called_with('PATCH',
                                             api_root + 'good/123',
                                             json={'href': 'good/123'},
                                             auth=DEFAULT_AUTH)
        self.goodie.bitter = 'sweet'
        self.goodie.patch()
        self.request_mock.assert_called_with('PATCH',
                                             api_root + 'good/123',
                                             json={'href': 'good/123',
                                                   'bitter': 'sweet'},
                                             auth=DEFAULT_AUTH)

    def test_post(self):
        # POST does not require the new location in the data.
        # It submits to the class href.
        self.goodie.post()
        self.request_mock.assert_called_with('POST',
                                             api_root + 'good/',
                                             json={},
                                             auth=DEFAULT_AUTH)
        self.goodie.bitter = 'sweet'
        self.goodie.post()
        self.request_mock.assert_called_with('POST',
                                             api_root + 'good/',
                                             json={'bitter': 'sweet'},
                                             auth=DEFAULT_AUTH)

    def test_put(self):
        # PUT requires the resource location among the data.
        with self.assertRaises(ValueError):
            self.goodie.put()
        # All attributes are submitted to the instance href.
        self.goodie.href = 'good/123'
        self.goodie.put()
        self.request_mock.assert_called_with('PUT',
                                             api_root + 'good/123',
                                             json={'href': 'good/123'},
                                             auth=DEFAULT_AUTH)
        self.goodie.bitter = 'sweet'
        self.goodie.put()
        self.request_mock.assert_called_with('PUT',
                                             api_root + 'good/123',
                                             json={'href': 'good/123',
                                                   'bitter': 'sweet'},
                                             auth=DEFAULT_AUTH)

    # Other methods
    # =============

    def test_delta(self):
        goodie_a = GoodResource(a='a')
        goodie_ab = GoodResource(a='a', b='b')
        goodie_bc = GoodResource(b='b', c='c')
        goodie_ha = GoodResource(a='a', href='good/123/')

        # No difference with self.
        delta = Resource.delta(goodie_a, goodie_a)
        self.assertDictEqual(delta.__dict__, {})

        # Difference is addition of b.
        delta = Resource.delta(goodie_a, goodie_ab)
        self.assertDictEqual(delta.__dict__, {'b': 'b'})

        # Removal of an attribute is not supported, so the user is warned.
        with patch('alwaysdata_api.log.warning') as mock_warning:
            delta = Resource.delta(goodie_ab, goodie_bc)
        self.assertDictEqual(delta.__dict__, {'c': 'c'})
        mock_warning.assert_called()

        # `href` on the instance is always included in the delta.
        delta = Resource.delta(goodie_ha, goodie_ha)
        self.assertDictEqual(delta.__dict__, {'href': 'good/123/'})

    def test_from_json(self):
        # The correct class is returned with all properties set.
        json = {'a': 'a', 'b': 123}
        res = GoodResource.from_json(json)
        self.assertIsInstance(res, GoodResource)
        self.assertDictEqual(res.__dict__, json)

    def test_request(self):
        # method: str, endpoint: str, auth: AuthBase,
        # **kwargs: Union[str, Dict, Sequence]) -> requests.Response:

        # URL is formed correctly.
        GoodResource.request('GET', 'good/123', auth=DEFAULT_AUTH)
        self.request_mock.assert_called_with('GET',
                                             api_root + 'good/123',
                                             auth=DEFAULT_AUTH)

        # The `auth` argument is handled correctly.
        GoodResource.request('GET', 'good/123', auth=(1, 2))
        self.request_mock.assert_called_with('GET',
                                             api_root + 'good/123',
                                             auth=(1, 2))

        # The `auth` argument is required.
        with self.assertRaises(TypeError):
            GoodResource.request('GET', 'good/123')

        # The request method is handled correctly.
        GoodResource.request('POST', 'good/123', auth=DEFAULT_AUTH)
        self.request_mock.assert_called_with('POST',
                                             api_root + 'good/123',
                                             auth=DEFAULT_AUTH)

        # The request method must be supported.
        with self.assertRaises(AssertionError):
            GoodResource.request('fail', 'good/123', auth=DEFAULT_AUTH)

    # Bad subclass implementation
    # ===========================

    def test_bad(self):
        # Resource subclasses must define a value for `href`.
        with self.assertRaises(AttributeError):
            BadResource.list()

        with self.assertRaises(AttributeError):
            self.baddie.post()


class TestDomain(TestCase):
    @patch('alwaysdata_api.requests.request')
    def test_whois(self, request_mock: MagicMock):
        # The correct URL is invoked.
        res = Domain(href='domain/123/')
        res.whois()
        request_mock.assert_called_with('POST',
                                        api_root + 'domain/123/whois/',
                                        auth=DEFAULT_AUTH)


class TestRecord(TestCase):
    def test_init(self):
        # The initialiser converts dict arg to int value.
        res = Record(domain=1234)
        self.assertEqual(res.domain, 1234)
        res = Record(domain={'href': '/v1/domain/1234/'})
        self.assertEqual(res.domain, 1234)
        with self.assertRaises(KeyError):
            Record(domain={})


class TestSite(TestCase):
    def test_init(self):
        # The initialiser converts language specific versions to the
        # general `language_version` attribute.
        for lang in ('elixir', 'nodejs', 'php', 'ruby', 'python'):
            field = f'{lang}_version'
            res = Site(**{field: 'v1.0'})
            # The language version is stored in `language_version`
            self.assertEqual(res.language_version, 'v1.0')
            # and not under the language-specific field.
            self.assertNotIn(field, res.__dict__)
            # `language_version` is preferred over language-specific args.
            res = Site(**{'language_version': 'v1.0', field: 'v1.2'})
            self.assertEqual(res.language_version, 'v1.0')

    @patch('alwaysdata_api.requests.request')
    def test_restart(self, request_mock: MagicMock):
        # The correct URL is invoked.
        res = Site(href='site/123/')
        res.restart()
        request_mock.assert_called_with('POST',
                                        api_root + 'site/123/restart/',
                                        auth=DEFAULT_AUTH)


if __name__ == '__main__':
    main()
